import java.util.Scanner;

public class lab2 {
    static char[][] board;
    static char CurrenPlayer = 'X';

    static void CreateBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }

    static void printBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }

    static void switchPlayer() {
        CurrenPlayer = (CurrenPlayer == 'X') ? 'O' : 'X';
    }

    static boolean makeXO(int position) {
        int row = (position - 1) / 3;
        int col = (position - 1) % 3;
        if (position >= 1 && position <= 9 && board[row][col] == '-') {
            board[row][col] = CurrenPlayer;
            return false;
        }
        return true;
    }

    static boolean BoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    static boolean checkWin() {
        return checkRowWin() || checkColWin() || checkDiagonalWin();
    }

    static boolean checkRowWin() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] != '-' && board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                return true;
            }
        }
        return false;
    }

    static boolean checkColWin() {
        for (int i = 0; i < 3; i++) {
            if (board[0][i] != '-' && board[0][i] == board[1][i] && board[1][i] == board[2][i]) {
                return true;
            }
        }
        return false;
    }

    static boolean checkDiagonalWin() {
        return (board[0][0] != '-' && board[0][0] == board[1][1] && board[1][1] == board[2][2]) ||
                (board[0][2] != '-' && board[0][2] == board[1][1] && board[1][1] == board[2][0]);
    }

    static void GameSet() {
        board = new char[3][3];
        CurrenPlayer = 'X';
        CreateBoard();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean continueGame = false;
        System.out.println("Welcome to XO!");
        System.out.println("Player 1: X");
        System.out.println("Player 2: O");
        System.out.println("Let's start the game!\n");
        do {
            GameSet();
            while (!checkWin() && !BoardFull()) {
                printBoard();
                int position;
                do {
                    System.out.print("Player " + CurrenPlayer + ", enter your move (1-9): ");
                    position = sc.nextInt();
                } while (makeXO(position));

                if (checkWin()) {
                    System.out.println("Player " + CurrenPlayer + " wins!");
                } else if (BoardFull()) {
                    System.out.println("It's a draw!");
                }
                switchPlayer();
            }
            printBoard();
            boolean roop = false;
            do {
                System.out.print("Do you want to continue playing? (Y/N): ");
                String input = sc.next();
                char character = input.charAt(0);
                if (character == 'Y' || character == 'y') {
                    roop = false;
                    continueGame = true;
                } else if (character == 'N' || character == 'n') {
                    roop = false;
                    continueGame = false;
                } else {
                    roop = true;
                }
            } while (roop);
        } while (continueGame);
    }
}
